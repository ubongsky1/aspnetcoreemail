﻿﻿
using Microsoft.AspNetCore.Mvc;
using MimeKit;
using MimeKit.Text;
using SendEmailInAspNetCore.Helpers;
using SendEmailInAspNetCore.Models;
using System;
using System.Diagnostics;
using System.Net.Mail;

namespace SendEmailInAspNetCore.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }
        [HttpPost]
        public IActionResult Contact(ContactViewModel contactViewModel)
        {
            if (ModelState.IsValid)
            {
               

                try
                {
                    MailMessage msz = new MailMessage();
                    msz.From = new MailAddress(contactViewModel.Email,contactViewModel.Name);//Email which you are getting 
                                                         //from contact us page 
                    msz.To.Add("ubsky1@gmail.com");//Where mail will be sent 
                  
                    msz.Subject = contactViewModel.Subject;
                    msz.Body = contactViewModel.Message;
                    SmtpClient smtp = new SmtpClient();

                    smtp.Host = "smtp.gmail.com";

                    smtp.Port = 587;

                    smtp.Credentials = new System.Net.NetworkCredential
                    ("your email", "your password");

                    smtp.EnableSsl = true;

                    smtp.Send(msz);

                    ModelState.Clear();
                    ViewBag.Mess = "Message sent,Thanks you for Contacting us ";
                }
                catch (Exception ex)
                {
                    ModelState.Clear();
                    ViewBag.Message = $" Sorry we are facing Problem here {ex.Message}";
                }
            }
            return View();
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public IActionResult Welcome()
        {
            return View();
        }

    }
}
